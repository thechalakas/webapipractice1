//
//  SimpleWebAPI.swift
//  WebApiPractice1
//
//  Created by Jay on 28/08/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

import Foundation

//enum for each API endpoint
enum Method: String
{
    case headphonesJSON = "api/HeadphonesJSON"
}


struct SimpleWebAPI
{
    private static var baseURLString = "http://simplewebapi1webapp1.azurewebsites.net/"
    
    //this is the variable for a specific web call. this one is for headphones
    //http://simplewebapi1webapp1.azurewebsites.net/api/HeadphonesJSON
    //note that I am not actually using the parameters 
    //just following the book but making my own changes at the same time
    static var headphonesURL: URL
    {
        return SimpleWebURL(method: .headphonesJSON, parameters: ["temp" : "one,two"])
    }
    
    static var headphonesAddURL : URL
    {
        let path = "api/Headphones"
        let toSendURL = baseURLString + path
        let components = URLComponents(string: toSendURL)
        return components!.url!
    }
    
    //method that will build the return URL
    //TODO : see if you can replace the parameters with a single string for patch, instead of key value later
    private static func SimpleWebURL(method: Method, parameters: [String:String]?) -> URL
    {
        let path = "api/Headphones"
        let toCallURL = baseURLString + path
        let components = URLComponents(string: toCallURL)!  //the base is ready
        
        //add path for specific API
        //components.path = "api/HeadphonesJSON"
        
        
        
        return components.url!
    }
}

