//
//  ViewController.swift
//  WebApiPractice1
//
//  Created by Jay on 28/08/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{

    //alright, we need the Simple Web Store here so we can use it
    //this store is connected to this controller in the AppDelegate.swift file
    var store:SimpleWebStore!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    //action for the button get headphones
    @IBAction func button(_ sender: UIButton)
    {
        print("button for get headphones pressed")
        //lets call the API service.
        store.fetchHeadphonesJSON()
    }
    
    //action for the button send headphone
    @IBAction func button_send_headphone(_ sender: UIButton)
    {
        print("button for send headphone pressed")
        store.addHeadphones()
    }
}

