//
//  SimpleWebStore.swift
//  WebApiPractice1
//
//  Created by Jay on 28/08/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

import Foundation
import ObjectMapper

class SimpleWebStore
{
    //URLSession - like a session web manager
    //URLSESSIONTASK - tasks which do the actual web interaction, under the responsibility of URLSESSION
    
    //first we need a session
    private let session: URLSession =
    {
        let config = URLSessionConfiguration.default
        return URLSession(configuration: config)
    }()
    
    
    
    func addHeadphones()
    {
        let url = SimpleWebAPI.headphonesAddURL
        print (url)
        
        //first the body
        //looks like I have to use this
        //https://github.com/Hearst-DD/ObjectMapper#installation
        //lets create a dummy headphone
        let headphone = Headphone()
        headphone.name = "Headphone Jay"
        headphone.id = 15
        headphone.category = "changed category"
        headphone.warehousedetails.warehouseid = 9
        headphone.warehousedetails.warehousename = "changed warehouse"
        print (" just created a headphone thing")
        
        //lets convert that to json string
        //Convert a model object to a JSON string: let JSONString = user.toJSONString(prettyPrint: true)
        let jsonstring = headphone.toJSONString(prettyPrint: true)
        let jsonstring2 = headphone.toJSONString()
        print("here is the pretty json string of the object - ", jsonstring!)
        print("here is the non pretty json string of the object - ", jsonstring2!)

        //converting the json string back to object
        //Convert a JSON string to a model object: let user = User(JSONString: JSONString)
        //let headphone2 = Headphone(JSONString: jsonstring!)
        
        //data information found here - https://stackoverflow.com/questions/24039868/creating-nsdata-from-nsstring-in-swift?noredirect=1&lq=1
        //https://stackoverflow.com/questions/31937686/how-to-make-http-post-request-with-json-body-in-swift
        //let data = "any string".data(using: .utf8)
        let data = jsonstring2?.data(using: .utf8)

        var request = URLRequest(url: url)
        
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpBody = data
        request.httpMethod = "POST"
        
        let task = session.dataTask(with: request)
        {
            (data,response,error) -> Void in
            
            if let jsonData = data
            {
                //the below is json data without any formatting in display
                
                if let jsonString = String(data: jsonData, encoding:.utf8)
                {
                    //Convert a JSON string to a model object: let user = User(JSONString: JSONString)
                    _ = Headphone(JSONString: jsonString)
                    print(jsonString)
                }
                
                //this one has proper formatting
                do
                {
                    let jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: [])
                    print(jsonObject)
                }
                catch let error
                {
                    print ("error with the json Object serialization thing \(error)")
                }
            }
            else if let requestError = error
            {
                print("Some request error man - \(requestError)")
            }
            else
            {
                print("Some non request error")
            }

        }
        task.resume()
    }
    
    func fetchHeadphonesJSON()
    {
        let url = SimpleWebAPI.headphonesURL
        let request = URLRequest(url: url)
        let task = session.dataTask(with: request)
        {
            (data,response,error) -> Void in
            
            if let jsonData = data
            {
                //the below is json data without any formatting in display
                
                if let jsonString = String(data: jsonData, encoding:.utf8)
                {
                    //Convert a JSON string to a model object: let user = User(JSONString: JSONString)
                    _ = Headphone(JSONString: jsonString)
                    print(jsonString)
                }
                
                //this one has proper formatting
                do
                {
                    let jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: [])
                    print(jsonObject)
                }
                catch let error
                {
                    print ("error with the json Object serialization thing \(error)")
                }
            }
            else if let requestError = error
            {
                print("Some request error man - \(requestError)")
            }
            else
            {
                print("Some non request error")
            }
        }//end of task
        task.resume()
    }//end of fetchHeadphonesJSON
}
