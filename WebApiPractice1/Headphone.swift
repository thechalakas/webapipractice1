//
//  Headphone.swift
//  WebApiPractice1
//
//  Created by Jay on 28/08/17.
//  Copyright © 2017 the chalakas. All rights reserved.
//

/*
 class User: Mappable {
 var username: String?
 var age: Int?
 var weight: Double!
 var array: [Any]?
 var dictionary: [String : Any] = [:]
 var bestFriend: User?                       // Nested User object
 var friends: [User]?                        // Array of Users
 var birthday: Date?
 
 required init?(map: Map) {
 
 }
 
 // Mappable
 func mapping(map: Map) {
 username    <- map["username"]
 age         <- map["age"]
 weight      <- map["weight"]
 array       <- map["arr"]
 dictionary  <- map["dict"]
 bestFriend  <- map["best_friend"]
 friends     <- map["friends"]
 birthday    <- (map["birthday"], DateTransform())
 }
 }
 */

import Foundation
import ObjectMapper

//Class that will be used to map between Headphone JSON to Object and vice versa
//Uses the ObjectMapper library link here - https://github.com/Hearst-DD/ObjectMapper#installation
//https://stackoverflow.com/questions/44806608/objectmapper-nested-dynamic-keys
//https://stackoverflow.com/questions/37834751/how-to-map-this-json-using-objectmapper?rq=1

class Headphone : Mappable
{
    public var id: Int?
    public var name: String?
    public var category: String?
    public var price: Double!
    public var warehouseid: Int?
    public var warehousedetails = Warehouse()
    
    
    required init?(map: Map)
    {

    }
    
    init()
    {
        id = 10
        name = "default name"
        category = "default category"
        price = 10.0
        warehouseid = 5

    }
    
    func mapping(map: Map)
    {
        id <- map["Id"]
        name <- map["Name"]
        category <- map["Category"]
        price <- map["Price"]
        warehouseid <- map["WarehouseId"]
        warehousedetails <- map["Warehouse"]
    }
    
    //set default values
    

}

//this will be used in the above class Headphone
class Warehouse : Mappable
{
    public var warehouseid: Int?
    public var warehousename: String?
    
    required init?(map: Map)
    {
        
    }
    
    init()
    {
        warehousename = "default warehouse"
        warehouseid = 25
    }
    
    func mapping(map: Map)
    {
        warehouseid <- map["Id"]
        warehousename <- map["Warehouse_Name"]
    }
}
