# WebApiPractice1

1. This one is the complete package about POSTing and GETing from the WEB API. 

2. You need to open the project work space not the code proj like you normally do. 

3. You will get a lot of build errors. this has to do with the project using a older version of project mapper. the app still runs so you are okay :)

4. This project uses pods. 

**references and links**

1. https://cocoapods.org/#install

2. https://cocoapods.org/#get_started

3. http://cocoapods.org/pods/Moya


Update : This project uses web calls made using the default web library of iOS. Use this to understand how 
networking works in iOS. DONT USE THIS IN AN ACTUAL PROJECT TO CONSUME API. IF YOU ARE BUILDING AN APP
THAT NEEDS TO CONSUME AN API PLEASE USE a web library such as Moya (linked above)

I will be building a new repo that uses Moya and another Object Mapper (the mapper used in this repo is giving
a lot of build errors).


---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 